//
//  MapBoxManager.h
//  MapBoxBridgeExample
//
//  Created by Hosamane K N, Vinay on 7/31/17.
//  Copyright © 2017 Facebook. All rights reserved.
//

#import "React/RCTViewManager.h"
#import <React/RCTBridgeModule.h>
@import Mapbox;

@interface MapBoxManager : RCTViewManager <RCTBridgeModule>

@property(strong,nonatomic)NSDictionary *sourceLocation;
@property(strong,nonatomic)NSDictionary *destinationLocation;

@end
