//
//  MapBoxView.h
//  MapBoxBridgeExample
//
//  Created by Hosamane K N, Vinay on 7/31/17.
//  Copyright © 2017 Facebook. All rights reserved.
//

#import "React/RCTView.h"

@interface MapBoxView : RCTView

//- (instancetype)initWithLocation:(NSDictionary*)sourceLocation Destination:(NSDictionary*)destinationLocation;

@property(strong,nonatomic)NSDictionary *sourceLocation;
@property(strong,nonatomic)NSDictionary *destinationLocation;


-(void)setSourceLocation:(NSDictionary*)source;
-(void)setDestinationLocation:(NSDictionary*)destination;

@end
