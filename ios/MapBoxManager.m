//
//  MapBoxManager.m
//  MapBoxBridgeExample
//
//  Created by Hosamane K N, Vinay on 7/31/17.
//  Copyright © 2017 Facebook. All rights reserved.
//

#import "MapBoxManager.h"
#import "MapBoxView.h"
#import <MapKit/MapKit.h>


@interface MapBoxManager ()

@end

@implementation MapBoxManager

RCT_EXPORT_MODULE()
- (UIView *)view
{
  return [[MapBoxView alloc]init ];
}

RCT_CUSTOM_VIEW_PROPERTY(sourceLocation, json, MapBoxManager)
{
  [view setSourceLocation:[RCTConvert NSDictionary:json]] ;
}

RCT_CUSTOM_VIEW_PROPERTY(destinationLocation, json, MapBoxManager)
{
  [view setDestinationLocation:[RCTConvert NSDictionary:json]] ;
}

RCT_EXPORT_METHOD(startNavigation:(NSDictionary*)source Destination:(NSDictionary*)destination)
{
  self.sourceLocation= source;
  self.destinationLocation= destination;
  
  NSLog(@"%@",source);
  NSLog(@"%@",destination);
}




@end
