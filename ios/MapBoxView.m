//
//  MapBoxView.m
//  MapBoxBridgeExample
//
//  Created by Hosamane K N, Vinay on 7/31/17.
//  Copyright © 2017 Facebook. All rights reserved.
//

#import "MapBoxView.h"
@import Mapbox;
@import MapboxCoreNavigation;
@import MapboxDirections;
@import MapboxNavigation;
@import AVFoundation;
#import "MapBoxManager.h"

#import <React/RCTBridge.h>
#import <React/RCTEventDispatcher.h>
#import <React/UIView+React.h>

@interface MapBoxView () <AVSpeechSynthesizerDelegate,MBNavigationViewControllerDelegate>
@property (nonatomic, assign) CLLocationCoordinate2D destination;
@property (nonatomic) MBDirections *directions;
@property (nonatomic) MBRoute *route;
@property (nonatomic) MBRouteController *navigation;
@property (nonatomic) NSLengthFormatter *lengthFormatter;
@property (nonatomic) AVSpeechSynthesizer *speechSynth;
@property(strong,nonatomic) UIViewController *rootViewController;

@property(assign,nonatomic) CLLocationCoordinate2D source;

@end

@implementation MapBoxView
{
  MGLMapView *_map;
}


-(id)init{
  
  self = [super init];
  
  _map = [[MGLMapView alloc] initWithFrame:self.bounds];
  _map.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
  _map.showsUserLocation = YES;
  // _map.zoomLevel = 2;
  _map.userTrackingMode = MGLUserTrackingModeFollow;
  
  [self addSubview:_map];
  
  return self;
}


- (BOOL)navigationViewController:(MBNavigationViewController * _Nonnull)navigationViewController shouldRerouteFromLocation:(CLLocation * _Nonnull)location SWIFT_WARN_UNUSED_RESULT
{
  return true;
}

-(void)setSourceLocation:(NSDictionary*)source
{

  float latDest = [[source objectForKey:@"lat"] floatValue];
  float langDest = [[source objectForKey:@"long"] floatValue];
  
  //_source = CLLocationCoordinate2DMake(latDest, langDest);
  
}

-(void)setDestinationLocation:(NSDictionary*)destination
{
  //Addidng polyline
  
  _source = _map.userLocation.location.coordinate;
  
  
  float latSource = _source.latitude;
  float longSource = _source.longitude;
  
  float latDest = [[destination objectForKey:@"lat"] floatValue];
  float langDest = [[destination objectForKey:@"long"] floatValue];
  
    MBWaypoint *origin = [[MBWaypoint alloc]initWithCoordinate:CLLocationCoordinate2DMake(12.9213, 77.6844) coordinateAccuracy:-1 name:nil];
//  MBWaypoint *origin = [[MBWaypoint alloc]initWithCoordinate:CLLocationCoordinate2DMake(latSource, longSource) coordinateAccuracy:-1 name:nil];
  MBWaypoint *destinationLoc = [[MBWaypoint alloc]initWithCoordinate:CLLocationCoordinate2DMake(latDest, langDest) coordinateAccuracy:-1 name:nil];
  
  NSArray<MBWaypoint *> *waypoints = @[origin,destinationLoc];
  
  MBRouteOptions *options = [[MBRouteOptions alloc] initWithWaypoints:waypoints profileIdentifier:MBDirectionsProfileIdentifierAutomobileAvoidingTraffic];
  options.includesSteps = YES;
  options.routeShapeResolution = MBRouteShapeResolutionFull;
  
  NSURL *urlData = [[MBDirections sharedDirections] URLForCalculatingDirectionsWithOptions:options];
  
  NSData *data = [NSData dataWithContentsOfURL:urlData];
  
  NSDictionary *json =(NSDictionary*) [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
  
  NSArray *routes = [json objectForKey:@"routes"];
  
  
  NSLog(@"%@",[routes firstObject]);
  
  NSLog(@"%@",json);
  
  if (_map.annotations) {
    [_map removeAnnotations:_map.annotations];
  }
  
  NSMutableArray *pointAnnotations = [NSMutableArray arrayWithCapacity:2];
  
  CLLocationCoordinate2D coordinate1 = CLLocationCoordinate2DMake(12.9213, 77.6844);
  MGLPointAnnotation *point1 = [[MGLPointAnnotation alloc] init];
  point1
  .coordinate = coordinate1;
  point1.title = [NSString stringWithFormat:@"%.f, %.f", coordinate1.latitude, coordinate1.longitude];
  [pointAnnotations addObject:point1];
  
  CLLocationCoordinate2D coordinate2= CLLocationCoordinate2DMake(12.9795, 77.5909);
  MGLPointAnnotation *point2 = [[MGLPointAnnotation alloc] init];
  point2
  .coordinate = coordinate2;
  point2.title = [NSString stringWithFormat:@"%.f, %.f", coordinate2.latitude, coordinate2.longitude];
  [pointAnnotations addObject:point2];
  
  MGLCoordinateBounds bounds = MGLCoordinateBoundsMake(coordinate1, coordinate2);
  
  [_map setVisibleCoordinateBounds:bounds];
  
  MBRoute *route = [[MBRoute alloc] initWithJson:[routes firstObject] waypoints:waypoints routeOptions:options];
  
  CLLocationCoordinate2D *routeCoordinates = malloc(route.coordinateCount * sizeof(CLLocationCoordinate2D));
  
  [route getCoordinates:routeCoordinates];
  
  MGLPolyline *polyline = [MGLPolyline polylineWithCoordinates:routeCoordinates count:route.coordinateCount];
  
  [_map addAnnotation:polyline];
  [_map addAnnotations:pointAnnotations];
  
  [_map setVisibleCoordinates:routeCoordinates count:route.coordinateCount edgePadding:UIEdgeInsetsZero animated:YES];
  
  free(routeCoordinates);
  
  self.route = route;
  
  MBNavigationViewController *controller = [[MBNavigationViewController alloc] initWithRoute:route directions:[MBDirections sharedDirections] style:nil locationManager:nil];
  
    controller.pendingCamera = _map.camera;
  
    self.rootViewController = [[[UIApplication sharedApplication] keyWindow] rootViewController];
  
    [self.rootViewController presentViewController:controller animated:YES completion:nil];

}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
