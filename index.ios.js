// index.ios.js
import React, { Component } from 'react';

import {
  AppRegistry,
  StyleSheet,
  Text,
  View,
  Image,
  TextInput,
  Button,
  TouchableHighlight,
  NativeModules
} from 'react-native';

import { requireNativeComponent } from 'react-native';

var MapBoxManager = NativeModules.MapBoxManager;

import MapBox from './MapBox'

class MapBoxBridgeExample extends Component {

  _onPressStartNavigation(){
    MapBoxManager.startNavigation({lat:1234 , long:5678} , {lat:731837,long:82787482});
  }

  componentWillMount()
  {
     //MapBoxManager.startNavigation({lat:1234 , long:5678} , {lat:731837,long:82787482});
}

  render() {
    return (
      <View style={styles.container}>
        <MapBox
        style={styles.map}
        sourceLocation = {{lat:12.9795,long:77.5909}}
        destinationLocation = {{lat:12.9795,long:77.5909}}
         />
        <Text style={styles.maptext}> MapBox Native Module Exapmle Project </Text>
      </View>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    backgroundColor: '#F5FCFF',
  },
  map: {
    flex: 0.5,
  },
  maptext: {
    flex: 0.25,
    textAlign:'center'
  },
  button: {
    flex: 0.25,
    textAlign:'center'
  }
});
AppRegistry.registerComponent('MapBoxBridgeExample', () => MapBoxBridgeExample);
