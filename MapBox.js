// MapBox.js
import { requireNativeComponent } from 'react-native';

// requireNativeComponent automatically resolves this to "RNTMapManager"
module.exports = requireNativeComponent('MapBox', null);